#!/usr/bin/env python3

from concurrent.futures import ProcessPoolExecutor
import os
import time
import json

import colorama
from colorama import Fore, Style
from jinja2 import Template
import requests
from unidecode import unidecode

from best_resolution import select_best_resolution
from m3u import parser
from chclassify import channel_classes, classify_channel

def stream_is_on(channel):
    name, url = channel
    status = 'OFFLINE'
    try:
        response = requests.get(url, stream=True, timeout=5)
        if response.status_code == 200:
            status = 'ONLINE'
    except:
        pass
    return {'name': name.strip(), 'url': url, 'status': status}


def parse_deluxe():
    DELUXE = 'https://raw.githubusercontent.com/habacuquemirray/limitedition/master/DELUXE2'
    m3u8 = requests.get(DELUXE).text
    premium1 = ''
    premium2 = False
    for n, line in enumerate(m3u8.splitlines()):
        if n > 10 and '-----' in line:
            break
        if line == '':
            continue
        premium1 = '\n'.join([premium1, line])

    m3u8_content = set(parser(premium1.strip()))
    print(f'{len(m3u8_content)} canais na playlist.')
    time.sleep(2)

    online_channels = []
    with ProcessPoolExecutor() as executor:
        for channel in executor.map(stream_is_on, m3u8_content):
            status_color = Fore.RED
            if channel['status'] == 'ONLINE':
                status_color = Fore.BLUE
                if channel not in online_channels:
                    online_channels.append(channel)

            print(f'{status_color}[{channel["status"]}] {Style.RESET_ALL}{channel["name"]}')
    return online_channels


if __name__ == '__main__':
    colorama.init(autoreset=True)

    channels = parse_deluxe()

    # filtra os canais selecionado a melhor resolução disponível
    channels = select_best_resolution(channels)


    # classifica os canais de acordo com o tipo da programação e agrupa
    ch_classification = {key: [] for key in channel_classes}
    print('Classificando os canais')
    for ch in channels:
        ch_class = classify_channel(channels[ch]['name'])
        print(channels[ch]['name'].ljust(25), ch_class)
        ch_classification[ch_class].append(channels[ch])


    # cria as playlists por tipo de programação
    for ch_class in ch_classification:
        channels = ch_classification[ch_class]
        if channels != []:
            print('Criando playlist {}'.format(ch_class))
            template = Template(open('channels.j2').read())
            channels_menu = template.render(channels=channels)
            playlist_name = unidecode(ch_class.replace('/', '-'))
            path = './playlist/'
            with open(''.join([path, playlist_name]), 'w') as f:
                f.write(channels_menu)

    os.system('git add .')
    os.system('git commit -m "channels update"')
    os.system('git push')
