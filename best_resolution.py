def get_resolution(channel_name):
    RESOLUTIONS = {'SD': 0, 'HD': 1, 'FHD': 2}
    channel_resolution = channel_name[-3:].strip()
    return RESOLUTIONS.get(channel_resolution, 0)


def select_best_resolution(channels):
    SD = 0
    HD = 1
    FHD = 2

    best_channels = {}
    for ch in channels:
        name = ch['name']
        ch_resolution = get_resolution(name)
        if ch_resolution == SD and name.endswith(' SD'):
            name = name.replace(' SD', '').strip()
        if ch_resolution == HD and name.endswith(' HD'):
            name = name.replace(' HD', '').strip()
        if ch_resolution == FHD and name.endswith(' FHD'):
            name = name.replace(' FHD', '').strip()

        if name in best_channels and ch_resolution > best_channels[name]['resolution']:
            best_channels[name]['url'] = ch['url']
            best_channels[name]['resolution'] = ch_resolution
        else:
            best_channels[name] = {'name': name, 'url': ch['url'],
                                   'resolution': ch_resolution}
    return best_channels
