from difflib import SequenceMatcher
import json


channel_classes = ('Adultos', 'Canais Abertos', 'Documentários',
                   'Entretenimento/Música', 'Esportes', 'Infantil', 'Filmes',
                   'Notícias', 'Séries', 'Variedades')


def classify_channel(channel_name):
    channel_classes = [
        {'name': 'Adultos',
         'canais': ('PLAYBOY', 'SEXY HOT', 'SEXTREME', 'VENUS')},

        {'name': 'Canais Abertos',
         'canais': ('CULTURA', 'GLOBO', 'GLOBO BRASILIA', 'GLOBO MINAS',
                    'GLOBO BELO HORIZONTE', 'GLOBO TV TEM', 'SBT', 'BAND',
                    'REDETV',
                    'RECORD', 'GAZETA', 'REDE VIDA', 'CANCAO NOVA', 'TV BRASIL',
                    'TV CAMARA', 'TV JUSTICA', 'TV SENADO', 'NBR',
                    'TV APARECIDA')},

        {'name': 'Documentários',
         'canais': ('ANIMAL PLANET', 'DISCOVERY', 'DISCOVERY CIVILIZATION',
                    'DISCOVERY WORLD', 'DISCOVERY SCIENCE', 'FUTURA', 'HISTORY',
                    'NAT GEO', 'NATIONAL GEOGRAPHIC',
                    'INVESTIGATION DISCOVERY')},

        {'name': 'Entretenimento/Música',
         'canais': ('BIS', 'MTV', 'MULTISHOW', 'MUSIC BOX', 'VH1')},

        {'name': 'Esportes',
         'canais': ('COMBATE', 'ESPORTE INTERATIVO', 'ESPN', 'ESPN BRASIL',
                    'SPORTV', 'FOX SPORTS', 'BAND SPORTS', 'PREMIERE',
                    'REAL MADRID')},

        {'name': 'Infantil',
         'canais': ('BABY TV', 'GLOOB', 'GLOOBINHO', 'DISNEY CHANNEL',
                    'NICK JR', 'CARTOON NETWORK', 'BOOMERANG', 'DISCOVERY KIDS',
                    'TV RÁ TIM BUM', 'NAT GEO KIDS', 'NICKELODEON',
                    'TOONCAST', 'ZOOMOO')},

        {'name': 'Filmes',
         'canais': ('TCM', 'TNT', 'CANAL BRASIL', 'MEGAPIX', 'PRIME BOX',
                    'TELECINE', 'CINEMAX', 'PARAMOUNT', 'SPACE')},

        {'name': 'Notícias',
         'canais': ('GLOBONEWS', 'BANDNEWS', 'BLOOMBERG', 'RECORDNEWS', 'BCC',
                    'CNN')},

        {'name': 'Séries',
         'canais': ('AMC', 'WARNER', 'WARNER CHANNEL', 'FX', 'AXN', 'A&E',
                    'SYFY', 'TNT SERIES',
                    'UNIVERSAL CHANNEL', 'TBS', 'SONY', 'FOX',
                    'COMEDY CENTRAL', 'LIFETIME', 'HBO', 'HBO SIGNATURE')},

        {'name': 'Variedades',
         'canais': ('VIVA', 'GNT', 'TLC', 'ARTE 1', 'FISH TV', 'FOOD NETWORK',
                    'DISCOVERY HOME & HEALTH', 'E!', 'FOX LIFE', 'CURTA!',
                    'OFF', 'TRUETV')},
        ]

    max_similar = 0
    channel_class = None
    match_name = channel_name.upper()

    for chclass in channel_classes:
        similaridade = map(lambda x: SequenceMatcher(None, match_name, x).ratio(), chclass['canais'])
        similaridade_classe = max(similaridade)
        if similaridade_classe > max_similar:
            max_similar = similaridade_classe
            channel_class = chclass['name']
        if similaridade_classe == 1:
            break

    #print('Canal:', channel_name,
    #      'Classificação:', channel_class,
    #      'Similaridade:', max_similar)
    return channel_class
