'''
    m3u playlist parser.
'''
from pathlib import Path
import os
import re

import requests


def format_token(token):
    upper = ('ESPN', 'FHD', 'HD', 'SD', 'TV', 'SP', 'RJ')
    if token.upper() in upper or len(token) == 3:
        token = token.upper()
    else:
        token = token.title()
    return token.strip()


def parse_extinf(extinf):
    try:
        inf = extinf.strip()
        infname, infurl = inf.split(',', 1)[1].splitlines()
        last = inf.rfind(',')
        infname = infname[infname.rfind(',') + 1:]
        tokens = infname.split(' ')
        tokens = [format_token(i) for i in tokens if i != ' ']
        infname = ' '.join(tokens).replace('Fullhd', 'FHD')
        infname = infname.replace('Full HD', 'FHD')
        return infname.strip(), infurl
    except:
        return ''


def parser(filename, sort_by_name=True, timeout=2):
    ''' Parse a m3u playlist from url of file '''

    if isinstance(filename, str) and filename.startswith('http'):
        m3u = requests.get(filename, timeout=timeout).text
    elif isinstance(filename, str) and filename.startswith('#EXTM3U'):
        m3u = filename
    else:
        with open(filename) as f:
            m3u = f.read()

    M3U_HEADER = '#EXTM3U'
    if m3u.startswith(M3U_HEADER) is False:
        raise TypeError('Invalid m3u file.')

    extinfs = re.finditer('#EXTINF.*?\n.*?\n', m3u, re.DOTALL)

    parsed_data = [parse_extinf(extinf.group()) for extinf in extinfs]
    parsed_data = [i for i in parsed_data if i != '']

    if sort_by_name:
        parsed_data = sorted(parsed_data, key=lambda x: x[0])

    return parsed_data
